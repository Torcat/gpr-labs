#include <iostream>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
#include "LaplacianSmoothing.h"


void LaplacianSmoothing::setMesh(TriangleMesh *newMesh) {
	mesh = newMesh;
}

/* This method should apply nIterations iterations of the laplacian vector multiplied by lambda 
   to each of the vertices. */
void LaplacianSmoothing::iterativeLaplacian(int nIterations, float lambda) {
	int counter = 0;
	while (counter < nIterations) {
		counter++;
		for (unsigned int i = 0; i < mesh->getVertices().size(); i++) {
			glm::vec3 pi = mesh->getVertices()[i];
			vector<unsigned int> neighbors;
			mesh->getNeighbors(i, neighbors);
			glm::vec3 laplacianPi = glm::vec3(0);
			for (unsigned j = 0; j < neighbors.size(); j++) {
				glm::vec3 pj = mesh->getVertices()[neighbors[j]];
				laplacianPi += pj - pi;
			}
			laplacianPi /= neighbors.size();
			glm::vec3 piP = pi + lambda * laplacianPi;
			mesh->getVertices()[i] = piP;
		}
	}
}

/* This method should apply nIterations iterations of the bilaplacian operator using lambda 
   as a scaling factor. */
void LaplacianSmoothing::iterativeBilaplacian(int nIterations, float lambda){
	int counter = 0;
	while (counter < nIterations) {
		counter++;
		for (unsigned int i = 0; i < mesh->getVertices().size(); i++) {
			glm::vec3 pi = mesh->getVertices()[i];
			vector<unsigned int> neighbors;
			mesh->getNeighbors(i, neighbors);
			glm::vec3 laplacianPi = glm::vec3(0);
			for (unsigned j = 0; j < neighbors.size(); j++) {
				glm::vec3 pj = mesh->getVertices()[neighbors[j]];
				laplacianPi += pj - pi;
			}
			laplacianPi /= neighbors.size();
			glm::vec3 piP = pi + lambda * laplacianPi;
			glm::vec3 laplacianPiP = glm::vec3(0);
			for (unsigned j = 0; j < neighbors.size(); j++) {
				glm::vec3 pj = mesh->getVertices()[neighbors[j]];
				laplacianPiP += pj - piP;
			}
			laplacianPiP /= neighbors.size();
			glm::vec3 p1SecondP = piP - lambda * laplacianPiP;
			mesh->getVertices()[i] = p1SecondP;
		}
	}
}

/* This method should apply nIterations iterations of Taubin's operator using lambda 
   as a scaling factor, and computing the corresponding nu value. */
void LaplacianSmoothing::iterativeLambdaNu(int nIterations, float lambda) {
	int counter = 0;
	float nuFactor = lambda / (0.1 * lambda - 1);
	while (counter < nIterations) {
		counter++;
		for (unsigned int i = 0; i < mesh->getVertices().size(); i++) {
			glm::vec3 pi = mesh->getVertices()[i];
			vector<unsigned int> neighbors;
			mesh->getNeighbors(i, neighbors);
			glm::vec3 laplacianPi = glm::vec3(0);
			for (unsigned j = 0; j < neighbors.size(); j++) {
				glm::vec3 pj = mesh->getVertices()[neighbors[j]];
				laplacianPi += pj - pi;
			}
			laplacianPi /= neighbors.size();
			glm::vec3 piP = pi + lambda * laplacianPi;
			glm::vec3 laplacianPiP = glm::vec3(0);
			for (unsigned j = 0; j < neighbors.size(); j++) {
				glm::vec3 pj = mesh->getVertices()[neighbors[j]];
				laplacianPiP += pj - piP;
			}
			laplacianPiP /= neighbors.size();
			glm::vec3 piSecondP = piP + nuFactor * laplacianPiP;
			mesh->getVertices()[i] = piSecondP;
		}
	}
}

/* This method should compute new vertices positions by making the laplacian zero, while
   maintaing the vertices marked as constraints fixed. */
void LaplacianSmoothing::globalLaplacian(const vector<bool> &constraints) {
	int nVertices = mesh->getVertices().size();
	Eigen::SparseMatrix<double> MatrixL(nVertices, nVertices);
	Eigen::SparseMatrix<double> Minv(nVertices, nVertices);
	Eigen::SparseMatrix<double> C(nVertices, nVertices);
	vector<unsigned int> neighbors;

	for (unsigned int i = 0; i < nVertices; i++) {
		mesh->getNeighbors(i, neighbors);
		Minv.insert(i, i) = 1.0/neighbors.size();
		for (unsigned j = 0; j < neighbors.size(); j++) {
			C.coeffRef(i, neighbors[j]) = 1.0;
		}
		C.coeffRef(i, i) = -(double)neighbors.size();
	}

	Eigen::LeastSquaresConjugateGradient<Eigen::SparseMatrix<double> > solver;
	MatrixL = Minv * C;
	Eigen::MatrixXd S = Eigen::MatrixXd::Zero(nVertices,3);
	for (unsigned int i = 0; i < nVertices; i++) {
		if (constraints[i]) {
			MatrixL.row(i) *= 0; // SET ROW TO ZERO
			MatrixL.coeffRef(i, i) = 1.0; // SET DIAGONAL TO ONE
			S(i,0) = mesh->getVertices()[i].x;
			S(i,1) = mesh->getVertices()[i].y;
			S(i,2) = mesh->getVertices()[i].z;
		}
	}
	MatrixL.prune(0, 0);
	solver.compute(MatrixL);
	Eigen::MatrixXd pPrime(nVertices,3);
	Eigen::MatrixXd Guess = Eigen::MatrixXd::Zero(nVertices, 3);
	for (unsigned int i = 0; i < nVertices; i++) {
		Guess(i, 0) = mesh->getVertices()[i].x;
		Guess(i, 1) = mesh->getVertices()[i].y;
		Guess(i, 2) = mesh->getVertices()[i].z;
	}
	pPrime = solver.solve(S);
	for (unsigned int i = 0; i < nVertices; i++) {
		mesh->getVertices()[i] = glm::vec3(pPrime(i, 0), pPrime(i, 1), pPrime(i, 2));
	}	
}

/* This method has to optimize the vertices' positions in the least squares sense, 
   so that the laplacian is close to zero and the vertices remain close to their 
   original locations. The constraintWeight parameter is used to control how close 
   the vertices have to be to their original positions. */
void LaplacianSmoothing::globalBilaplacian(const vector<bool> &constraints, float constraintWeight) {
	int nVertices = mesh->getVertices().size();
	int nConstraints = std::count(constraints.begin(), constraints.end(), true);
	Eigen::SparseMatrix<double> MatrixL(nVertices, nVertices);
	Eigen::SparseMatrix<double> MInv(nVertices, nVertices);
	Eigen::SparseMatrix<double> C(nVertices, nVertices);
	vector<unsigned int> neighbors;
	for (unsigned int i = 0; i < nVertices; i++) {
		mesh->getNeighbors(i, neighbors);
		MInv.insert(i, i) = 1.0/neighbors.size();
		for (unsigned j = 0; j < neighbors.size(); j++) {
			C.coeffRef(i, neighbors[j]) = 1.0;
		}
		C.coeffRef(i, i) = -(double)neighbors.size();
	}
	MatrixL = MInv * C;
	
	Eigen::MatrixXd S = Eigen::MatrixXd::Zero(nVertices + nConstraints, 3);
	Eigen::SparseMatrix<double> MatrixLNew(nVertices + nConstraints, nVertices);
	Eigen::MatrixXd matL(nVertices + nConstraints, nVertices);
	matL << Eigen::MatrixXd(MatrixL), Eigen::MatrixXd::Zero(nConstraints, nVertices);
	MatrixLNew = matL.sparseView();
	int constraintsCounter = 0;
	for (unsigned int i = 0; i < nVertices; i++) {
		if (constraints[i]) {
			MatrixLNew.insert(nVertices + constraintsCounter, i) = 1.0 * constraintWeight;
			S(nVertices + constraintsCounter, 0) = mesh->getVertices()[i].x * constraintWeight;
			S(nVertices + constraintsCounter, 1) = mesh->getVertices()[i].y * constraintWeight;
			S(nVertices + constraintsCounter, 2) = mesh->getVertices()[i].z * constraintWeight;
			constraintsCounter++;
		}
	}
	Eigen::LeastSquaresConjugateGradient<Eigen::SparseMatrix<double> > lscg;
	lscg.compute(MatrixLNew);
	Eigen::MatrixXd pPrime(nVertices, 3);
	Eigen::MatrixXd Guess = Eigen::MatrixXd::Zero(nVertices, 3);
	for (unsigned int i = 0; i < nVertices; i++) {
		Guess(i, 0) = mesh->getVertices()[i].x;
		Guess(i, 1) = mesh->getVertices()[i].y;
		Guess(i, 2) = mesh->getVertices()[i].z;
	}
	pPrime = lscg.solveWithGuess(S, Guess);
	for (unsigned int i = 0; i < nVertices; i++) {
		mesh->getVertices()[i] = glm::vec3(pPrime(i, 0), pPrime(i, 1), pPrime(i, 2));
	}
}
