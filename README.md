
# GEOMETRY PROCESSING LABS

This repository contains a series of labs focused on geometry processing. The content is organized into different folders, each dedicated to specific topics and labs.

## Directory Structure

- **01-icp-base**
  - This folder includes the following labs:
     - **Lab1:** PCA (Principal Component Analysis)
     - **Lab2:** ICP (Iterative Closest Point)
- **02-reconstruction-base**
  - Contains Lab3:
    - **Reconstruction**
- **03-curvatures-base**
  - Contains Lab4:
    - **Curvatures**
- **04-smoothing-base**
  - Contains Lab5:
    - **Smoothing**
- **06-parameterization-base**
  - Contains Lab6:
    - **Parameterization**

## Project Execution on Linux Distributions

The project is configured to be run on Linux distributions. Below are the instructions to install necessary dependencies for compiling and executing the labs, particularly for Debian-based distributions.

### Installing Dependencies

1. **Update System**
   ```
   sudo apt-get update
   ```

2. **Build Essentials**
   ```
   sudo apt-get install build-essentials
   ```

3. **CMake**
   ```
   sudo apt-get install cmake
   ```

4. **GLFW**
   ```
   sudo apt-get install libglfw3
   sudo apt-get install libglfw3-dev
   ```

5. **GLM**
   ```
   sudo apt-get install libglm-dev
   ```

## Execution Instructions

Each folder contains specific instructions for running the corresponding lab. Please refer to the individual folder for detailed guidance.

---

**Note:** Ensure to follow the instructions in each lab folder carefully to successfully execute the labs.