
# LAB 3 - Execution Instructions

This project must be run on a Linux distribution and requires `cmake` to be installed on the system.

## Steps for Execution

1. Open a terminal in Linux in the current directory.

2. Create a new directory for the build and navigate to it:
   ```
   mkdir build
   cd build
   ```

3. Run `cmake` to configure the project:
   ```
   cmake ..
   ```

4. Compile the project using `make`:
   ```
   make -j
   ```

5. Run the program with the necessary arguments. For example:
   ```
   ./02-reconstruction-base <path1>
   ```

   ### Example
   To run the program with example files, use:
   ```
   ./02-reconstruction-base ../../points/frog.ply
   ```