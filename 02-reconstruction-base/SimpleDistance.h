#ifndef _SIMPLE_DISTANCE_INCLUDE
#define _SIMPLE_DISTANCE_INCLUDE


#include <glm/glm.hpp>
#include "ImplicitFunction.h"
#include "PointCloud.h"
#include "NearestNeighbors.h"


class SimpleDistance : public ImplicitFunction
{

public:
	void init(const PointCloud *pointCloud, float samplingRadius);

	bool operator()(const glm::vec3 &P, float &value) const;
	
private:
	NearestNeighbors knn;
	const PointCloud *cloud;
	float radius = 0.0f;
	float rhoValue = 1.0f;
	float deltaValue = 1.0f;
};


#endif // _SIMPLE_DISTANCE_INCLUDE



