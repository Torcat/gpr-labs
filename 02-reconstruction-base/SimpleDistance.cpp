#include "SimpleDistance.h"


/* Initialize everything to be able to compute the implicit distance of [Hoppe92] 
   at arbitrary points that are close enough to the point cloud.
 */

void SimpleDistance::init(const PointCloud *pointCloud, float samplingRadius)
{
	cloud = pointCloud;
	knn.setPoints(&pointCloud->getPoints());
	radius = samplingRadius;
}


/* This operator returns a boolean that if true signals that the value parameter
   has been modified to contain the value of the implicit function of [Hoppe92]
   at point P.
 */

bool SimpleDistance::operator()(const glm::vec3 &P, float &value) const {
	vector<pair<size_t, float>> neighborIds;
	knn.getNeighborsInRadius(P, radius, neighborIds);
	int i = neighborIds[0].first;
	glm::vec3 pi = cloud->getPoints()[i];
	glm::vec3 ni = cloud->getNormals()[i];
	glm::vec3 z = pi - (glm::dot((P - pi), ni))*ni;

	if (glm::length(z - pi) <= deltaValue + rhoValue) {
		value = glm::dot((P - pi), ni);
		return true;
	}
	return false;
}






