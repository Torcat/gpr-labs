#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
#include "Parameterizer.h"
#include "timing.h"

#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;


// This method should compute new texture coordinates for the input mesh
// using the harmonic coordinates approach with uniform weights.
// The 'TriangleMesh' class has a method 'getTexCoords' that may be used 
// to access and update its texture coordinates per vertex.

void Parameterizer::harmonicCoordinates(TriangleMesh *mesh) {	
	// 1) First we need to determine the border edges of the input mesh
	// TODO
	
	// 2) Then, these edges need to be arranged into a single cycle, the border polyline
	// TODO

	// 3) Each of the vertices on the border polyline will receive a texture coordinate
	// on the border of the parameter space ([0,0] -> [1,0] -> [1,1] -> [0,1]), using 
	// the chord length approach
	// TODO
	
	// 4) We build an equation system to compute the harmonic coordinates of the 
	// interior vertices
	// TODO
	
	// 5) Finally, we solve the system and assign the computed texture coordinates
	// to their corresponding vertices on the input mesh
	// TODO


	// ---------------------------------------------------- BEGIN 1 ----------------------------------------------------
	int nVertices = mesh->getVertices().size();
	set<pair<int, int>> borderEdges;
	int nTriangles = mesh->getTriangles().size();
	for (int i = 0; i < nTriangles; i+=3) {
		int firstT = mesh->getTriangles()[i]; // CURRENT TRIANGLE
		int secondT = mesh->getTriangles()[i+1]; // NEXT TRIANGLE

		auto it1 = std::find_if(borderEdges.begin(), borderEdges.end(), [firstT, secondT](const pair<int, int>& p) { return p.first == firstT && p.second == secondT; });
		auto it2 = std::find_if(borderEdges.begin(), borderEdges.end(), [firstT, secondT](const pair<int, int>& p) { return p.first == secondT && p.second == firstT; });
		if (it1 != borderEdges.end()) {
			borderEdges.erase(it1);
		}
		else if(it2 != borderEdges.end()) {
			borderEdges.erase(it2);
		}
		else {
			borderEdges.insert(pair<int, int>(firstT, secondT));
		}

		firstT = mesh->getTriangles()[i+1]; // UPDATE CURRENT TRIANGLE
		secondT = mesh->getTriangles()[i+2]; // UPDATE NEXT TRIANGLE
		it1 = std::find_if(borderEdges.begin(), borderEdges.end(), [firstT, secondT](const pair<int, int>& p) { return p.first == firstT && p.second == secondT; });
		it2 = std::find_if(borderEdges.begin(), borderEdges.end(), [firstT, secondT](const pair<int, int>& p) { return p.first == secondT && p.second == firstT; });
		if (it1 != borderEdges.end()) {
			borderEdges.erase(it1);
		}
		else if (it2 != borderEdges.end()) {
			borderEdges.erase(it2);
		}
		else {
			borderEdges.insert(pair<int, int>(firstT, secondT));
		}

		firstT = mesh->getTriangles()[i+2];
		secondT = mesh->getTriangles()[i];
		it1 = std::find_if(borderEdges.begin(), borderEdges.end(), [firstT, secondT](const pair<int, int>& p) { return p.first == firstT && p.second == secondT; });
		it2 = std::find_if(borderEdges.begin(), borderEdges.end(), [firstT, secondT](const pair<int, int>& p) { return p.first == secondT && p.second == firstT; });
		if (it1 != borderEdges.end()) {
			borderEdges.erase(it1);
		}
		else if (it2 != borderEdges.end()) {
			borderEdges.erase(it2);
		}
		else {
			borderEdges.insert(pair<int, int>(firstT, secondT));
		}
	}

	// ---------------------------------------------------- END 1 ----------------------------------------------------


	// ---------------------------------------------------- BEGIN 2 ----------------------------------------------------
	vector<int> cycle;
	auto iterationValue = borderEdges.begin();

	while (!borderEdges.empty()) {
		int currentFirstT = iterationValue->first;
		int currentSecondT = iterationValue->second;
		cycle.push_back(iterationValue->first);
		borderEdges.erase(iterationValue);

		// UPDATE ITERATION VALUE (CONDITION)
		iterationValue = std::find_if(borderEdges.begin(), borderEdges.end(), [currentSecondT](const pair<int, int>& p) { return p.first == currentSecondT; });
		
		// CHECK IF THERE IS A NEXT ITERATION VALUE
		if (iterationValue == borderEdges.end()) {
			cout << "App is still running!! WAIT" << endl;
		}
	}
	// ---------------------------------------------------- END 2 ----------------------------------------------------
	

	// ---------------------------------------------------- BEGIN 3 ----------------------------------------------------
	double cycleLength = 0;
	for (int i = 0; i < cycle.size()-1; i++) {
		cycleLength += glm::length(mesh->getVertices()[cycle[i+1]] - mesh->getVertices()[cycle[i]]);
	}
	cycleLength += glm::length(mesh->getVertices()[cycle[cycle.size() - 1]] - mesh->getVertices()[cycle[0]]);
	double currentCycleLength = 0;
	Eigen::MatrixXd T = Eigen::MatrixXd::Zero(nVertices, 2);
	for (int i = 0; i < cycle.size()-1; i++) {
		double currentLength = glm::length(mesh->getVertices()[cycle[i+1]] - mesh->getVertices()[cycle[i]]);
		currentCycleLength += currentLength;
		if (currentCycleLength < cycleLength / 4.0) {
			T(cycle[i+1], 0) = currentCycleLength * 4.0/ cycleLength;
			T(cycle[i+1], 1) = 0.0;
		} else if (currentCycleLength < cycleLength / 2.0) {
			T(cycle[i+1], 0) = 1.0;
			T(cycle[i+1], 1) = (currentCycleLength - 1.0 / 4.0 * cycleLength) * 4.0 / cycleLength;
		} else if (currentCycleLength < 3.0 * cycleLength / 4.0) {
			T(cycle[i+1], 0) = 1.0 - (currentCycleLength - cycleLength / 2.0) * 4.0 / cycleLength;
			T(cycle[i+1], 1) = 1.0;
		} else {
			T(cycle[i+1], 0) = 0.0;
			T(cycle[i+1], 1) = 1.0 - (currentCycleLength - 3.0 / 4.0 * cycleLength) * 4.0 / cycleLength;
		}
	}
	T(cycle[0], 0) = 0.0;
	T(cycle[0], 1) = 0.0;
	// ---------------------------------------------------- END 3 ----------------------------------------------------


	// ---------------------------------------------------- BEGIN 4 ----------------------------------------------------
	Eigen::SparseMatrix<double> L(nVertices, nVertices);
	Eigen::SparseMatrix<double> MInv(nVertices, nVertices);
	Eigen::SparseMatrix<double> C(nVertices, nVertices);

	vector<unsigned int> neighbors;
	for (unsigned int i = 0; i < nVertices; i++) {
		mesh->getNeighbors(i, neighbors);
		MInv.insert(i, i) = 1.0 / neighbors.size();
		for (unsigned j = 0; j < neighbors.size(); j++) {
			C.coeffRef(i, neighbors[j]) = 1.0;
		}
		C.coeffRef(i, i) = -(double)neighbors.size();
	}
	
	L = MInv * C;
	for (int i = 0; i < cycle.size() - 1; i++) {
		L.row(cycle[i]) *= 0;
		L.coeffRef(cycle[i], cycle[i]) = 1.0;
	}
	L.prune(0, 0);
	// ---------------------------------------------------- END 4 ----------------------------------------------------


	// ---------------------------------------------------- BEGIN 5 ----------------------------------------------------
	// https://eigen.tuxfamily.org/dox/classEigen_1_1LeastSquaresConjugateGradient.html
	Eigen::LeastSquaresConjugateGradient<Eigen::SparseMatrix<double> > solver;
	solver.compute(L);
	Eigen::MatrixXd TPrime(nVertices, 2);
	TPrime = solver.solve(T);
	for (unsigned int i = 0; i < nVertices; i++) {
		mesh->getTexCoords()[i] = glm::vec2(TPrime(i, 0), TPrime(i, 1));
	}
	// ---------------------------------------------------- END 5 ----------------------------------------------------
}
