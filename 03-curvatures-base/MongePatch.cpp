#include <iostream>
#include "MongePatch.h"


// Given a point P, its normal, and its closest neighbors (including itself) 
// compute a quadratic Monge patch that approximates the neighborhood of P.
// The resulting patch will be used to compute the principal curvatures of the 
// surface at point P.

void MongePatch::init(const glm::vec3 &P, const glm::vec3 &normal, const vector<glm::vec3> &closest) {
	vertice = P;
	VS.clear();
	glm::vec3 W = -normal;
	W = glm::normalize(W);
	glm::vec3 U = glm::cross(glm::vec3(1.0,0.0,0.0),W);
	U = glm::normalize(U);
	glm::vec3 V = glm::cross(W,U);
	V = glm::normalize(V);

	vector<glm::vec3> newSystemPi(closest.size());
	for (int i = 0; i < closest.size(); i++) {
		newSystemPi[i] = glm::vec3(glm::dot(U, closest[i] - P),glm::dot(V, closest[i] - P), glm::dot(W, closest[i] - P));
	}

	Eigen::MatrixXd MatrixA = Eigen::MatrixXd::Zero(6,6);
	Eigen::VectorXd s;
	Eigen::VectorXd b = Eigen::VectorXd::Zero(6);
	Eigen::VectorXd qi(6);
	for (unsigned int i = 0; i < closest.size(); i++) {
		qi(0) = newSystemPi[i].x * newSystemPi[i].x;
		qi(1) = newSystemPi[i].x * newSystemPi[i].y;
		qi(2) = newSystemPi[i].y * newSystemPi[i].y;
		qi(3) = newSystemPi[i].x;
		qi(4) = newSystemPi[i].y;
		qi(5) = 1;

		MatrixA += qi * qi.transpose();
		b += newSystemPi[i].z * qi;
	}
	Eigen::ColPivHouseholderQR<Eigen::MatrixXd> dec(MatrixA);
	s = dec.solve(b);

	for (int i = 0; i < 6; i++) {
		VS.push_back(s[i]);
	}
}

// Return the values of the two principal curvatures for this patch
void MongePatch::principalCurvatures(float &kmin, float &kmax) const {
	// kmin = 0.f;
	// kmax = 0.f;

	Eigen::Matrix2d Hw;
	Hw(0, 0) = 2*VS[0];
	Hw(0, 1) = VS[1];
	Hw(1, 0) = VS[1];
	Hw(1, 1) = 2*VS[2];

	Eigen::JacobiSVD<Eigen::Matrix2d, Eigen::ComputeThinU | Eigen::ComputeThinV> svd(Hw);
	Eigen::Vector2d principalCurvature = svd.singularValues();

	kmin = principalCurvature[0];
	kmax = principalCurvature[1];
}
