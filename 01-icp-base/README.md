
# LAB 1 - Instrucciones de Ejecución

This project must be run on a Linux distribution and requires `cmake` to be installed on the system.

## Pasos para la Ejecución

1. Open a terminal in Linux in the current directory.

2. Create a new directory for the build and navigate to it:
   ```
   mkdir build
   cd build
   ```

3. Run `cmake` to configure the project:
   ```
   cmake ..
   ```

4. Compile the project using `make`:
   ```
   make -j
   ```

5. Run the program with the necessary arguments. For example:
   ```
   ./01-icp-base <path1> <path2>
   ```

   ### Example
   To run the program with example files, use:
   ```
   ./01-icp-base ../../scans/armadillo/scan0.ply ../../scans/armadillo/scan1.ply
   ```