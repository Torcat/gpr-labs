#include <iostream>
#include <algorithm>
#include "IterativeClosestPoint.h"
#include <Eigen/Core>
#include <Eigen/SVD>
#include <glm/gtc/matrix_transform.hpp>

#define KNEIGHBORS 8

void IterativeClosestPoint::setClouds(PointCloud *pointCloud1, PointCloud *pointCloud2) {
	cloud1 = pointCloud1;
	cloud2 = pointCloud2;
	knn.setPoints(&(cloud1->getPoints()));
}

// This method should mark the border points in cloud 1. It also changes their color (for example to red).
// You will need to add an attribute to this class that stores this property for all points in cloud 1. 
void IterativeClosestPoint::markBorderPoints() {
	const vector<glm::vec3>* points1 = &(cloud1->getPoints());

	for (unsigned int i = 0; i < cloud1->getPoints().size(); i++) {
		glm::vec3 point = cloud1->getPoints()[i];

		unsigned int kNeighbors = KNEIGHBORS;
		vector<size_t> neighborIds;
		vector<float> dSquared;
		knn.getKNearestNeighbors(point, kNeighbors, neighborIds, dSquared);

		Eigen::Matrix3f MatrixL;
		Eigen::Vector3f VectorD;
		performPCA(*points1, neighborIds, dSquared, MatrixL, VectorD);

		// Transform the k-nearest neighbors to the local reference frame
		vector<glm::vec3> neighborsF(neighborIds.size());
		for (unsigned int j = 0; j < neighborIds.size(); j++) {
			glm::vec3 pj_pi = points1->at(neighborIds[j]) - point;
			neighborsF[j] = glm::vec3(
				glm::dot(pj_pi, glm::vec3(MatrixL(0, 0), MatrixL(0, 1), MatrixL(0, 2))),
				glm::dot(pj_pi, glm::vec3(MatrixL(1, 0), MatrixL(1, 1), MatrixL(1, 2))),
				glm::dot(pj_pi, glm::vec3(MatrixL(2, 0), MatrixL(2, 1), MatrixL(2, 2)))
			);
		}

		for (unsigned int j = 0; j < neighborIds.size(); j++) {
			glm::vec3 pj_pi = points1->at(neighborIds[j]) - point;
			neighborsF[j] = glm::vec3(pj_pi.x,pj_pi.y,0); // X,Y,Z -> X,Y,0
		}

		vector<float> polarAngles(neighborIds.size());
		for (unsigned int j = 0; j < neighborIds.size(); j++) {
			polarAngles[j] = atan2(neighborsF[j].y, neighborsF[j].x);
		}

		std::sort(polarAngles.begin(), polarAngles.end());

		// Check if the angle between two consecutive neighbors is greater than a threshold
		int borderPoint = 0;
		for (unsigned int j = 0; j < neighborIds.size()-1; j++) {
			if (polarAngles[j+1] - polarAngles[j] > alphaThreshold) {
				borderPoint = 1;
				cloud1->getColors()[i] = glm::vec4(1.0,0.0,0.0,1.0); // RED
			}
		}
		borderPoints.push_back(borderPoint);
	}
}

// This method should compute the closest point in cloud 1 for all non border points in cloud 2. 
// This correspondence will be useful to compute the ICP step matrix that will get cloud 2 closer to cloud 1.
// Store the correspondence in this class as the following method is going to need it.
// As it is evident in its signature this method also returns the correspondence. The application draws this if available.
vector<int> *IterativeClosestPoint::computeCorrespondence() {
	correspondence.clear();
	unsigned int kNeighbors = KNEIGHBORS;
	vector<size_t> neighborIds;
	vector<float> dSquared;
	bool knnFound = false;
	int closestPointId = -1;
	for (int i = 0; i < cloud2->getPoints().size(); i++) {
		// search with knn not border point
		knn.getKNearestNeighbors(cloud2->getPoints()[i], kNeighbors, neighborIds, dSquared);
		int j;
		for (j = 0; j < kNeighbors; j++) {
			if (!borderPoints[neighborIds[j]]) {
				knnFound = true;
				closestPointId = neighborIds[j];
				break;
			}
		}
		
		if (!knnFound) {
			float minDistance = std::numeric_limits<float>::max();
			for (j = 0; j < cloud1->getPoints().size(); j++) {
				float currentDistance = glm::length(cloud2->getPoints()[i] - cloud1->getPoints()[j]);
				if (!borderPoints[j] && minDistance > currentDistance) {
					minDistance = currentDistance;
					closestPointId = j;
				}
			}
		}		
		correspondence.push_back(closestPointId);
	}
	
	return &correspondence;
}


// This method should compute the rotation and translation of an ICP step from the correspondence
// information between clouds 1 and 2. Both should be encoded in the returned 4x4 matrix.
// To do this use the SVD algorithm in Eigen.
glm::mat4 IterativeClosestPoint::computeICPStep() {
	const vector<glm::vec3>* points1 = &(cloud1->getPoints());
	const vector<glm::vec3>* points2 = &(cloud2->getPoints());

	glm::vec3 centroidQ = calculateCentroid(*points2);
	vector<glm::vec3> originQPoint = centerPoints(*points2, centroidQ);

	glm::vec3 centroidP = calculateCentroid(*points1, *points2, correspondence);
	vector<glm::vec3> originPPoint = centerPoints(*points1, centroidP, *points2, correspondence);

	Eigen::MatrixX3f Q(originPPoint.size(),3), P(originPPoint.size(),3);
	for (unsigned int i = 0; i < originQPoint.size(); i++) {
		Q(i, 0) = originQPoint[i][0];
		Q(i, 1) = originQPoint[i][1];
		Q(i, 2) = originQPoint[i][2];
		P(i, 0) = originPPoint[i][0];
		P(i, 1) = originPPoint[i][1];
		P(i, 2) = originPPoint[i][2];
	}
	Eigen::MatrixXf S = Q.transpose() * P;

	Eigen::JacobiSVD<Eigen::MatrixXf> eigensolver(S, Eigen::ComputeThinU | Eigen::ComputeThinV);
	Eigen::MatrixXf U = eigensolver.matrixU();
	Eigen::MatrixXf V = eigensolver.matrixV();

	Eigen::MatrixXf R = U * V.transpose();
	R.conservativeResizeLike(Eigen::MatrixXf::Identity(4,4));

	// REFLECTION
	if (S.determinant() < 0) {
		// Modify the last column of V (or U)
		V.col(V.cols()-1) *= -1;
		// Recompute R with the adjusted V
		R = U * V.transpose();
		R.conservativeResizeLike(Eigen::MatrixXf::Identity(4,4));
	}

	Eigen::Vector4f t = Eigen::Vector4f(centroidP.x, centroidP.y, centroidP.z, 1) - R * Eigen::Vector4f(centroidQ.x, centroidQ.y, centroidQ.z, 1);
	auto result = glm::mat4(R(0, 0), R(0, 1), R(0, 2), t(0),R(1, 0), R(1, 1), R(1, 2), t(1), R(2, 0), R(2, 1), R(2, 2), t(2), R(3, 0), R(3, 1), R(3, 2), R(3,3));
	
	return result;
}

// This method should check if the ICP algorithm has converged.
bool IterativeClosestPoint::checkConvergence(glm::mat4 matrix) {
	glm::mat3 R;
	R[0] = glm::vec3(matrix[0][0], matrix[0][1], matrix[0][2]);
	R[1] = glm::vec3(matrix[1][0], matrix[1][1], matrix[1][2]);
	R[2] = glm::vec3(matrix[2][0], matrix[2][1], matrix[2][2]);
	glm::vec3 t = glm::vec3(matrix[0][3], matrix[1][3], matrix[2][3]);
	glm::mat3 R_I = R - glm::mat3(1.f);
	float fNormal = 0.f;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			fNormal += R_I[i][j] * R_I[i][j];
		}
	}
	fNormal = sqrt(fNormal);
	return glm::length(t) < convergenceThreshold && fNormal < convergenceThreshold;
}

// This method should perform the whole ICP algorithm with as many steps as needed.
// It should stop when maxSteps are performed, when the Frobenius norm of the transformation matrix of
// a step is smaller than a small threshold, or when the correspondence does not change from the 
// previous step.
vector<int> *IterativeClosestPoint::computeFullICP(unsigned int maxSteps) {
	bool hasConverged = false;
	unsigned int currentStep = 0;
	vector<int>* currentCorrespondence = NULL;
	while (!hasConverged && currentStep < maxSteps) {
		currentStep++;
		currentCorrespondence = computeCorrespondence();
		glm::mat4 matrix = computeICPStep();
		cloud2->transform(matrix);
		hasConverged = checkConvergence(matrix);
	}
	return currentCorrespondence;
}
