// #ifndef _ITERATIVE_CLOSEST_POINT_INCLUDE
// #define _ITERATIVE_CLOSEST_POINT_INCLUDE


// #include "PointCloud.h"
// #include "NearestNeighbors.h"
// #include "MathUtils.h"


// class IterativeClosestPoint
// {

// public:
// 	void setClouds(PointCloud *pointCloud1, PointCloud *pointCloud2);
	
// 	void markBorderPoints();
// 	vector<int> *computeCorrespondence();
// 	glm::mat4 computeICPStep();
	
// 	vector<int> *computeFullICP(unsigned int maxSteps = 100);
	
// private:
// 	PointCloud *cloud1, *cloud2;
// 	NearestNeighbors knn;
// 	// Border points and correspondence vectors
//     std::vector<int> borderPointsCloud1;
//     std::vector<int> borderPointsCloud2;
//     std::vector<int> correspondence;
//     // Thresholds for convergence and border point detection
//     float convergenceThreshold = 0.1f;
//     float maxDeltaAlphaThreshold = 2.0f;

// };


// #endif // _ITERATIVE_CLOSEST_POINT_INCLUDE

#ifndef _ITERATIVE_CLOSEST_POINT_INCLUDE
#define _ITERATIVE_CLOSEST_POINT_INCLUDE

#include <limits>
#include "PointCloud.h"
#include "NearestNeighbors.h"
#include "MathUtilsPCA.h"

class IterativeClosestPoint
{

public:
	void setClouds(PointCloud *pointCloud1, PointCloud *pointCloud2);
	
	void markBorderPoints();
	vector<int> *computeCorrespondence();
	glm::mat4 computeICPStep();
	vector<int> *computeFullICP(unsigned int maxSteps = 100);
	
private:
	PointCloud *cloud1;
	PointCloud *cloud2;
	NearestNeighbors knn;

	vector<int> borderPoints;
	vector<int> correspondence;
	float convergenceThreshold = 0.2;
	float alphaThreshold = 1.f;
	unsigned int kNeighbors = 8;

	bool checkConvergence(glm::mat4 matrix);

};


#endif // _ITERATIVE_CLOSEST_POINT_INCLUDE





