#ifndef MATH_UTILS_PCA_INCLUDE
#define MATH_UTILS_PCA_INCLUDE

#include <vector>
#include <glm/glm.hpp>
#include <Eigen/Dense>

using namespace std;

glm::vec3 calculateCentroid(const vector<glm::vec3>& points);
glm::vec3 calculateCentroid(const vector<glm::vec3>& points1, const vector<glm::vec3>& points2, vector<int> &correspondence);
glm::vec3 calculateCentroid(const vector<glm::vec3>& points, vector<size_t>& neighborIds);
vector<glm::vec3> centerPoints(const vector<glm::vec3>& points, glm::vec3 centroid);
vector<glm::vec3> centerPoints(const vector<glm::vec3>& points1, glm::vec3 centroid, const vector<glm::vec3>& points2, vector<int>& correspondence);
vector<glm::vec3> centerNeighbors(const vector<glm::vec3>& points, glm::vec3 centroid, vector<size_t>& neighborIds);
void performPCA(const vector<glm::vec3>& points, vector<size_t>& neighborIds, vector<float>& dists_squared, Eigen::Matrix3f &MatrixL, Eigen::Vector3f &VectorD);

#endif // MATH_UTILS_PCA_INCLUDE