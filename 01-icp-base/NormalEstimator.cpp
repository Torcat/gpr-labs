#include "NormalEstimator.h"
#include "NearestNeighbors.h"


// This method has to compute a normal per point in the 'points' vector and put it in the 
// 'normals' vector. The 'normals' vector already has the same size as the 'points' vector. 
// There is no need to push_back new elements, only to overwrite them ('normals[i] = ...;')
// In order to compute the normals you should use PCA. The provided 'NearestNeighbors' class
// wraps the nanoflann library that computes K-nearest neighbors effciently. 

void NormalEstimator::computePointCloudNormals(const vector<glm::vec3> &points, vector<glm::vec3> &normals) {
	NearestNeighbors nearestNeighbors;
	nearestNeighbors.setPoints(&points);

	for (int i = 0; i < points.size(); i++) {
		// GETTING NEIGHBORS
		vector<float> dSquared;
		vector<size_t> neighborIds;
		nearestNeighbors.getKNearestNeighbors(points[i], kNeighbors, neighborIds, dSquared);

		// APPLYING PCA
		Eigen::Matrix3f MatrixL;
		Eigen::Vector3f VectorD;
		performPCA(points, neighborIds, dSquared, MatrixL, VectorD);
		glm::vec3 normal = glm::vec3(MatrixL(0, 0), MatrixL(1, 0), MatrixL(2, 0));
		if (normals[i].z < 0) {
			normal = -normal;
		} else {
			normals[i] = normal;
		}
	}
}
