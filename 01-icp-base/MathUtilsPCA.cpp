#include "MathUtilsPCA.h"

glm::vec3 calculateCentroid(const vector<glm::vec3>& points) {
	if (points.size() == 0) {
		return glm::vec3(0.0, 0.0, 0.0);
	}
	glm::vec3 pCentroid(0.0, 0.0, 0.0);
	for (unsigned int i = 0; i < points.size(); i++) {
		pCentroid += points[i];
	}
	return pCentroid /= points.size();
}

glm::vec3 calculateCentroid(const vector<glm::vec3>& points, vector<size_t>& neighborIds) {
	glm::vec3 pCentroid(0.0, 0.0, 0.0);
	for (unsigned int i = 0; i < neighborIds.size(); i++) {
		pCentroid += points[neighborIds[i]];
	}
	return pCentroid /= neighborIds.size();
}

glm::vec3 calculateCentroid(const vector<glm::vec3>& points1, const vector<glm::vec3>& points2, vector<int>& correspondence) {
	glm::vec3 pCentroid(0.0, 0.0, 0.0);
	for (unsigned int i = 0; i < points2.size(); i++) {
		pCentroid += points1[correspondence[i]];
	}
	return pCentroid /= points2.size();
}

vector<glm::vec3> centerPoints(const vector<glm::vec3>& points, glm::vec3 centroid) {
	if (points.size() == 0) {
		return vector<glm::vec3>();
	}
	vector<glm::vec3> centeredPoints(points.size());
	for (unsigned int i = 0; i < points.size(); i++) {
		centeredPoints[i] = points[i] - centroid;
	}
	return centeredPoints;
}

vector<glm::vec3> centerPoints(const vector<glm::vec3>& points1, glm::vec3 centroid, const vector<glm::vec3>& points2, vector<int>& correspondence) {
	vector<glm::vec3> centeredPoints(points2.size());
	for (unsigned int i = 0; i < points2.size(); i++) {
		centeredPoints[i] = points1[correspondence[i]] - centroid;
	}
	return centeredPoints;
}

vector<glm::vec3> centerNeighbors(const vector<glm::vec3>& points, glm::vec3 centroid, vector<size_t>& neighborIds) {
	vector<glm::vec3> centeredNeighbors(neighborIds.size());
	for (unsigned int i = 0; i < neighborIds.size(); i++) {
		centeredNeighbors[i] = points[neighborIds[i]] - centroid;
	}
	return centeredNeighbors;
}

void performPCA(const vector<glm::vec3>& points, vector<size_t>& neighborIds, vector<float>& dists_squared, Eigen::Matrix3f& MatrixL, Eigen::Vector3f& VectorD) {
	glm::vec3 pCentroid = calculateCentroid(points, neighborIds);
	vector<glm::vec3> originNeighborPoint = centerNeighbors(points, pCentroid, neighborIds);
	Eigen::Matrix3f C;
	C = Eigen::Matrix3f::Zero();
	for (unsigned int i = 0; i < neighborIds.size(); i++) {
		C += Eigen::Vector3f(originNeighborPoint[i][0], originNeighborPoint[i][1], originNeighborPoint[i][2]) * Eigen::Vector3f(originNeighborPoint[i][0], originNeighborPoint[i][1], originNeighborPoint[i][2]).transpose();
	}

	Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigensolver(C);
	MatrixL = eigensolver.eigenvectors();
	VectorD = eigensolver.eigenvalues();
}